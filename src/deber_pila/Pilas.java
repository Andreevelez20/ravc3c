/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package deber_pila;

import javax.swing.JOptionPane;
import java.io.IOException;

/**
 *
 * @author rodri
 */
class Pilas {
    private static final int MAX_LENGTH = 5;
	    private static String pila1[] = new String[MAX_LENGTH];
	    private static String pila2[]= new String[MAX_LENGTH];
	    private static int cima1 = -1;
	    private static int cima2 =-1;
	    private static String pilaaux1[] = new String[MAX_LENGTH];
	    private static int cimaaux1 = -1;
	    private static String pilaaux2[] = new String[MAX_LENGTH];
	    private static int cimaaux2 = -1;
	    

	        public void Insertar(){
	        
	           String entra= JOptionPane.showInputDialog("Digite un dato para la pila #1"); 
	        		   
	           Apilar(entra);
	        }

	        public void Apilar(String dato){
	          if ((pila1.length-1)==cima1){
	        	  JOptionPane.showMessageDialog(null,"Capacidad de la pila #1 al limite");
	            Imprimir();
	          }else{
	                cima1++;
	                        
	                JOptionPane.showMessageDialog(null,"Cima 1 en la posición "+cima1);
	                pila1[cima1]=dato;
	                
	          }
	        }

	        public  void Apilaraux(String dato){
	          if ((pilaaux1.length-1)==cimaaux1){
	            JOptionPane.showMessageDialog(null,"Capacidad de la pila auxiliar #1 al limite");
	          }else{
	             cimaaux1++;
	             pilaaux1[cimaaux1]=dato;
	           }
	        }

	        public  boolean vaciaaux(){
	            return (cimaaux1==-1);
	        }

	        public  boolean vacia(){
	            if (cima1==-1){
	                return (true);
	            }
	            else {
	                return(false);
	            }
	        }

	        public void Imprimir(){
	          String quedata,salida=" ";
	          if (cima1!=-1)
	          { do {
	                quedata=Desapilar();
	                salida=salida+quedata+"\n"; 
	                
	                Apilaraux(quedata);            
	            }while(cima1!=-1);
	            do {
	                quedata=Desapilaraux();
	                Apilar(quedata);
	            }while(cimaaux1!=-1);
	            JOptionPane.showMessageDialog(null, salida);
	          }
	          else {
	              JOptionPane.showMessageDialog(null, "La pila #1 esta vacía");
	          }
	        }

	        public String Desapilar(){
	          String quedato;
	          if(vacia()){
	              JOptionPane.showMessageDialog(null,"No se puede eliminar, pila #1 vacía !!!" );
	              return("");
	          }else{
	                  quedato=pila1[cima1];
	              pila1[cima1] = null;
	              --cima1;
	                  return(quedato);
	                }
	        }

	        public String Desapilaraux(){
	          String quedato;
	          if(cimaaux1== -1){
	                JOptionPane.showMessageDialog(null,"No se puede eliminar, pila #1 vacía !!!" );
	                return("");
	          }else{
	                  quedato=pilaaux1[cimaaux1];
	              pilaaux1[cimaaux1] = null;
	              --cimaaux1;
	                  return(quedato);
	               }
	        }

	        public  void Buscar(){
	            if (vacia()){
	                JOptionPane.showMessageDialog(null, "La pila #1 esta vacìa");
	            }
	            else{
	                String cad = JOptionPane.showInputDialog("Digite la cadena a buscar: ");
	                String quedata;
	                int bandera=0; 
	                do {
	                        quedata=Desapilar();
	                        if(cad.equals(quedata)){
	                            bandera=1; 
	                        }
	                        Apilaraux(quedata);            
	                }while(cima1!=-1);
	                do {
	                        quedata=Desapilaraux();
	                        Apilar(quedata);
	                }while(cimaaux1!=-1);
	                if (bandera==1) {
	                        JOptionPane.showMessageDialog(null,"Elemento encontrado");
	                }else{
	                        JOptionPane.showMessageDialog(null,"Elemento no encontrado :(");
	                }
	            }
	        }
	        public int contar() {
	            String quedata;
	            int contador = 0;
	            if (cima1!=-1)
	              { do {
	                    quedata=Desapilar();
	                    contador = contador+1;
	                    Apilaraux(quedata);            
	                }while(cima1!=-1);
	                do {
	                    quedata=Desapilaraux();
	                    Apilar(quedata);
	                }while(cimaaux1!=-1);
	                JOptionPane.showMessageDialog(null,"Elementos en la pila #1 : "+ contador);
	              }
	            else {
	                JOptionPane.showMessageDialog(null, "La pila #1 esta vacìa");
	            }return contador;
                }
    
}
